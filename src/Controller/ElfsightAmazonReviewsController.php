<?php

namespace Drupal\elfsight_amazon_reviews\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightAmazonReviewsController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/amazon-reviews/?utm_source=portals&utm_medium=drupal&utm_campaign=amazon-reviews&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
